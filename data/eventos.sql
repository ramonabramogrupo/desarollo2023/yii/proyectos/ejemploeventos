﻿DROP DATABASE IF EXISTS eventos2023;
CREATE DATABASE eventos2023;
USE eventos2023;

CREATE TABLE evento(
id int,
codigoReserva varchar(100) NOT NULL,
nombre varchar(300),
descripcion text,
fechaHora datetime,
fechaRelease date,
PRIMARY KEY(id),
UNIQUE KEY(codigoReserva)
);

CREATE TABLE sala(
id int,
nombre varchar(400),
descripcion text,
PRIMARY KEY(id)
);

CREATE TABLE tieneLugar(
id int,
idEvento int,
idSala int,
montajeSala varchar(400),
PRIMARY KEY(id),
UNIQUE KEY(idEvento,idSala)
);

ALTER TABLE tieneLugar 
  ADD CONSTRAINT fkTieneEvento FOREIGN KEY (idEvento) REFERENCES evento(id),
  ADD CONSTRAINT fkTieneSala FOREIGN KEY (idSala) REFERENCES sala(id);